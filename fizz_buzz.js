/**
* Write a program which loops from 1 to 100.
* Print 'FIZZ' when the current number divisible by 3.
* Print 'BUZZ' when the current number divisible by 5.
* Print 'FIZZBUZZ' when the current number divisible by both 3 & 5.
* Print the iteration number or the current number if the number is not divisible by 3, 5 and both 3 & 5.
*/

const limit = 100;
let counter = 1;
while (counter <= 100) {
	let output = ''
	if (counter % 3 == 0) {
		output += 'FIZZ';
	}
	if (counter % 5 == 0) {
		output += 'BUZZ';
	}
	console.log(output || counter);
	counter++;
}
