/*
Draw a triangle using #.
#
##
###
####
#####
######
#######
*/
const limit = 10;
let row = '';
for (let x = 0; x < limit; x++) {
	for (let y = 0; y <= x; y++) {
		row += '#';
	}
	row += '\n';
}
console.log(row);


/**
* Another version 
*/
console.log('\n\n');
for (let line = '#'; line.length < 8; line+= '#') {
	console.log(line);
}
