/**
* Zero is even.
* One is odd.
* For any other number N, its evenness is same as N-2
*
* Define a recursive function isEven corresponding to this description.
* The function should accept a single parameter (a positive , whole number) and returns a Boolean.
* Test it on 50 75, See how it behaves on -1. Why? can you think of a way to fix this?
*/

const isEven = n => {
	if (n === 0) {
		return true;
	} else if (n === 1) {
		return false;
	} else if (n < 0) {
		return isEven(-n);
	} else {
		return isEven(n - 2);
	}
}
console.log(isEven(4));
