/**
* Write a function countBs that takes a string as its only argument and returns a number that indicates how many uppercase "B" characters are there in string.

* Next, write a function called countChar that behaves like countsBs, except it takes a second argument that indicates the character that is to be counted ( rather than counting only uppercase "B" characters). Rewrite countBs to make use of this new function.
*
*/

const countChar = (string, character) => {
	let count = 0;
	for (let i = 0; i < string.length; i++) {
		if (string[i] === character) {
			count++;
		}
	}
	return count;
};

console.log(countChar("rabbit", "b"));
