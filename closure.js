const multiplier = function (factor) {
	return function (number) {
		return number * factor;
	}
};
// minified version of above function
const multiplier_mini = factor => number => number * factor;

let twice = multiplier(2);
let thrice = multiplier(3);
let fours = multiplier_mini(4);
console.log(twice(5));
console.log(thrice(5));
console.log(fours(10));
