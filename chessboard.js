/*
Write a program that creates a string
that represents an 8x8 grid.
1 2 3 4 5 6 7 8
8 9 10 11 12 13 14 15
15 16 17 18 19 20 21 22
22 23 24 25 26 27 28 29
29 30 31 32 33 34 35 36
36 37 38 39 40 41 42 43
43 44 45 46 47 48 49 50
50 51 52 53 54 55 56 57
*/

/*
Program model - 1
*/
const boardSize = 8;
let counter = 1;
let limit = 1;
for (let i = 1; i <= boardSize; i++ ) {
	let row = '';
	while (counter < (limit + boardSize)) {
		if (counter % 2 == 0) {
			row += '# ';
		} else {
			row += ' ';
		}
		counter++;
	}
	limit = counter - 1;
	counter = limit;
	console.log(row);
}
/*
Program model - 2
*/
console.log('\n\n\n');
console.log('Different version for the same program');
console.log('\n\n\n');
let size = 8;
let board = '';
for (let y = 0; y < size; y++) {
	for (let x= 0; x < size; x++) {
		if ((x + y) % 2 == 0) {
			board += ' ';
		} else {
			board += '# ';
		}
	}
	board += '\n';
}
console.log(board);

/*
Progam model -3
*/
console.log('\n\n\n');
const n = 8;
let count = 1,
	pattern = "",
	chessStart = 1,
	chessStop = 8;

while (count <= n) {
	for (let j = chessStart + count; j <= chessStop + count; j++) {
		if (j % 2 === 0 ) {
			pattern += " ";
		} else {
			pattern += "#";
		}
	}
	pattern += "\n";
	count += 1;
}
console.log(pattern);
